package utility;

/**
 * Enum for directions.
 */
public enum Direction {

    /**
     * 
     */
    STRAIGHT,
    /**
     * 
     */
    LEFT,
    /**
     * 
     */
    RIGHT,
    /**
     *
     */
    BOX;

}
